\documentclass[openany]{book}

\usepackage{color}
\usepackage{polyemptty}

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[french]{babel}

% \usepackage{ifthen,amsmath,amstext,amssymb}
% \usepackage{textcomp} % so that the euro symbol works
% \usepackage{longtable,float,wrapfig,subfigure}
\usepackage{url} \urlstyle{sf}
\usepackage{graphics}
\usepackage{amsfonts}
\usepackage[hidelinks]{hyperref}

\newcommand{\extlink}{\raisebox{-0.2em}{\includegraphics[height=.9\baselineskip]{img/extlink.pdf}}}

\begin{document}
\noindent
\begin{minipage}{.95\linewidth}
\begin{center}

  {\huge\color{violet}\bf Guide des }\smallskip

  {\large\color{violet}\bf (futurs) (ex)}\smallskip

  {\huge\color{violet}\bf stagiaires de L3}\medskip

  Martin Quinson
\end{center}
\end{minipage}~\begin{minipage}{.05\linewidth}
  \rotatebox{90}{\footnotesize v220930}
\end{minipage}

\bigskip\bigskip

Ce document est une sorte de pense-bête visant à expliquer les
attendus et le fonctionnement du stage de L3 à l'ENS Rennes. Ce n'est
pas un texte de loi, et toute remarque permettant de l'améliorer est
bienvenue. 

Ce texte est rédigé au féminin pour éviter les lourdeurs de l'écriture
inclusive tout en en gardant l'esprit.

La mise en page est au format A5, de façon à pouvoir imprimer en deux pages par
page sans s'abimer les yeux, mais les URL sont clicables dans le pdf.
\url{https://framagit.org/mquinson/writings/blob/master/stage-L3/stagiaire.pdf}\extlink

\bigskip
Voir aussi le guide pour une {\color{violet}étude bibliographique efficace}:\\
\url{https://framagit.org/mquinson/writings/blob/master/stage-L3/biblio.pdf}\extlink


% Table des matières générales, et restore le style de chapitre ensuite
\begingroup
\let\cleardoublepage\relax
\let\clearpage\relax
\renewcommand\contentsname{\vspace{-15mm}}
\titleformat{\chapter}{}{}{0pt}{\color{violet}\Large\bf Table des matières}
\tableofcontents
\endgroup

\chapter{Objectifs du stage}
Vous avez déjà entrevu le monde de la recherche depuis votre arrivée à l'ENS. Lors des séminaires de
département, de la visite de labo ou même dans certains cours d'ouverture. Après cet aperçu de la largeur
thématique de l'informatique, votre stage d'été vous donne l'occasion de découvrir la recherche par la
pratique, en immersion dans une équipe.

\paragraph{Découvrir en pratique l'activité de recherche.} Vous allez
mener un petit projet dans une équipe de recherche. Il est rare que le
travail réalisé dans ce cadre mène à une publication scientifique
(même si ce n'est pas impossible), mais ce stage est l'occasion de
découvrir le fonctionnement d'un laboratoire de recherche. Le vrai
objectif réside dans cette intégration, et vous devez donc parler avec
les chercheurs que vous rencontrerez, car il est probable que vous les
revoyez pendant votre carrière.

\paragraph{Affiner vos choix thématiques.} Certaines connaissent
depuis toute petite leur future spécialisation en
informatique. D'autres (beaucoup plus nombreuses) ne savent même pas
exactement les choix qui s'offrent à elles. Le stage L3 est l'occasion
pour toutes de s'essayer à un thème de recherche sans risque: libre à
vous de continuer dans le même thème par la suite, ou de changer au
moment du stage M1 (ou plus tard).

N'envisagez pas votre cursus comme un arbre de spécialisation, mais plutôt comme
un ensemble d'expériences qui s'ajoutent. Il est parfois préférable d'être
mauvaise dans de nombreux domaines que sur-spécialisée.

\paragraph{Cadre réglementaire.} Le stage fait partie de la formation,
et il est régit par les modalités de contrôle connaissance. Il doit
durer au moins 6 semaines à plein temps, dans un laboratoire de
recherche public ou privé en France. Il donne lieu à un rapport, et à
une soutenance.

Il est indispensable d'avoir une convention d'accueil établie par l'école et
signée par le laboratoire d'accueil \textbf{avant} le début du stage. Établir ce
document prend plusieurs semaines, et il convient donc de s'y prendre à temps.

\chapter{Avant le stage}
\vspace{-\baselineskip}
\section{Trouver de l'inspiration}
Choisir sa thématique puis son équipe est une étape cruciale, qui mérite que vous preniez le temps de vous
documenter correctement. L'objectif est de trouver le domaine dans lequel vous vous épanouirez le plus. Le
sujet doit vous plaire, en évitant les domaines où vous avez trop de difficultés. Soyez passionnément
pragmatiques. Suivez points forts et envies, et fuyez les sujets arides pour vous. 

Partez des cours qui vous ont plu. Explorez les pages web de vos enseignantes et présentatrices préférées.
Parcourez leurs articles scientifiques. Visitez les sites web institutionnels et découvrez les rapports
d'activité Inria.

Identifier des scientifiques à qui demander un stage est très chronophage, mais choisir vite et mal aura très
probablement des conséquences désagréables.

\medskip
\begin{itemize}
% \item Prenez des idées dans la liste des stages collectés par les
%   collègues de l'ENS Lyon: \url{www.ens-lyon.fr/DI/stageL3/}
\item Liste thématique  des \href{http://www.inria.fr/recherches/structures-de-recherche/rechercher-une-equipe}{\violet{équipes Inria}\extlink} (lisez les Rapports d'Activité \href{https://raweb.inria.fr/}{\extlink})
\item Moteur de recherche des \href{http://annuaire.cnrs.fr/l3c/owa/annuaire.recherche/}{\violet{laboratoires
      CNRS}\extlink}
  \begin{itemize}
  \item \href{http://www.cnrs.fr/ins2i/}{\violet{INS2I}\extlink}:
    Institut des sciences de l'information et de leurs interactions
    (informatique, signal, image, automatique, robotique).
  \item \href{http://www.cnrs.fr/insis/}{\violet{INSIS}\extlink}: Institut des
    sciences de l'ingénierie et des systèmes\\ (sciences et technologies
    des automatismes, entre autres)
  \item \href{http://www.cnrs.fr/comitenational/sections/section.php?sec=06}%
    {\violet{Section 06}\extlink} fondements de
    l'informatique, calculs, algorithmes, représentations,
    exploitations
  \item \href{http://www.cnrs.fr/comitenational/sections/section.php?sec=07}%
    {\violet{Section 07}\extlink} traitements,
    systèmes intégrés matériel-logiciel, robots, commandes, images,
    contenus, interactions, signaux et langues
  \item Les travaux interdisciplinaires sont également intéressants.
  \end{itemize}
\item Moteurs de recherche des articles scientifiques (listes à jour
  des publications récentes de chacune): \url{dblp.uni-trier.de/} et
  \url{scholar.google.fr/}
\end{itemize}

\medskip
\violet{Ne contactez personne avant l'accord du responsable des stages.}

\section{Choisir son stage}
\paragraph{Critères de choix.}
Liste non exhaustive de critères souvent utilisés:
\begin{itemize}
\item \violet{Sujet.} Vous souhaitez travailler sur le sujet où vous vous épanouissez.
\item \violet{Encadrante.} Vous avez envie de travailler avec une personne en
  particulier. Il peut s'agir d'une enseignante, d'une oratrice du séminaire, etc. 
\item \violet{Géographie}. Vous avez envie de faire votre stage dans tel ou tel lieu.
\item \violet{Environnement.} Un grand laboratoire universitaire, un centre de recherche plus spécialisé, un
  laboratoire de recherche industrielle, etc.
\item Savoir saisir sa chance en évitant les effets d'aubaine est une gageure.
\end{itemize}
\vspace{-\baselineskip}

\paragraph{Procédure en pratique.}
\begin{itemize}
\item Tout se passe sur votre canal Discord personnel du serveur des stages.
\item Argumentez votre motivation sur votre canal, puis \violet{attendez la validation} des enseignantes avant
  de contacter votre encadrante potentielle.
\end{itemize}
\vspace{-\baselineskip}

\paragraph{Règles pour la validation des stages.}
\begin{itemize}
\item Pas plus d'une élève par équipe.
\item Vous devez être encadrée par une chercheuse expérimentée, active en recherche en informatique, visible
  au niveau international.
\item Il est souhaitable que l'équipe participe financièrement au
  stage, par une gratification pour les élèves non-fonctionnaires
  et/ou avec une aide en nature: tickets repas, participation au
  logement ou aux frais de transport, etc.  Si l'équipe ne veut pas
  vous payer, voudront-ils vous encadrer au quotidien? Cette règle
  est cependant moins stricte que les deux autres. À vous de voir.
\end{itemize}

\paragraph{Trouver la bonne encadrante.} Si tout se passe bien, vous
resterez longtemps en contact avec votre encadrante, qui écrira des
lettres de recommandation pour vos prochains stages et au delà. Il n'y
a pas de recette magique pour trouver la bonne personne. Vous cherchez
quelqu'un d'expérimentée afin qu'elle puisse vous expliquer les
méandres du métier, mais pas trop surchargée pour qu'elle ait
suffisamment de temps à vous consacrer. La perle rare.

Avoir une mauvaise encadrante rend le stage potentiellement plus compliqué et
désagréable, mais (1) il est difficile de déterminer à l'avance si on sera bien
encadrée (2) une bonne méthodologie de stagiaire peut compenser certains défauts
de l'encadrante.

\section{Contacter une chercheuse}
\paragraph{Être professionnelle.} Il convient de respecter les règles de
politesse (sans déférence excessive) dans vos communications. Vouvoiement,
``Bonjour'' en entrée, ``Cordialement'' en sortie, pas de fautes d'orthographe.
Dans certaines équipes, il est important de contacter le chef d'équipe en
premier.
  
\paragraph{Pas de candidature à la vanille.} Les chercheuses reçoivent
énormément de spam-candidatures, envoyées à l'identique tous
azimuts. Seules les mauvaises encadrantes (celles qui n'ont pas le
temps de faire les choses bien, pas même les recrutements) acceptent
ces candidatures à la vanille, et il faut donc faire sentir à votre
chercheuse que votre lettre est destinée à elle seule. Citez
explicitement son nom, son équipe (pas de ``votre prestigieux
établissement''). \textit{Extra points} si vous posez une question
intelligente sur l'une de ses publications en rapport avec le sujet
espéré de votre stage.

\paragraph{Éviter les mauvaises surprises.} Vous devez être reconnaissante à
votre encadrante de vous prendre en stage, mais il vous faut de plus l'assurance
que vous serez bien encadrée sur place. Une solution est de vous assurer que
l'équipe s'engage financièrement afin d'augmenter vos chances de suivi, mais le
plus simple est de vous assurer que votre encadrante pourra faire au moins une
voire deux réunions de une heure chaque semaine. Si vous êtes suivie par deux
personnes, assurez vous qu'elles ne seront pas parties en même temps.

\section{Établir une convention de stage}

\noindent
Il faudra remplir à temps le formulaire qui vous sera communiqué par Discord.
%l'adresse suivante: \\ \url{http://wiki.dit.ens-rennes.fr/DIT/Dokuwiki/doku.php/stage:procedure2018_19}

\vspace{-.5\baselineskip}
\paragraph{Questions administratives.} Le premier piège à éviter est de se
tromper entre les tutelles de votre future équipe (Inria, CNRS, universités,
etc): demandez à votre chercheuse et à son assistante. Soyez cohérente: la
directrice du centre Inria ne peut signer une convention établie avec le
laboratoire universitaire.

\paragraph{Durée de stage et vacances.} Il est impossible de faire moins de 6
semaines de stage; La durée maximale des stages est de 11 semaines (pour que
vous soyez bien reposée l'an prochain).
%
Il est préférable de prendre vos vacances après la fin du stage, mais si vous
tenez à couper avec une (seule) période de vacances, renseignez la période
complète du stage dans le formulaire, et indiquez les dates exactes de stages et
vacances dans un message séparé à Céline Vinson (avec Martin Quinson en CC).

\chapter{Pendant le stage}

\vspace{-3\baselineskip}\noindent%
(préférez toujours les consignes de votre encadrante aux conseils suivants)

\vspace{-.5\baselineskip}
\paragraph{Rythmes de travail.} Si votre convention indique 35h/semaine, vous
devez être présente au laboratoire 35h par semaine. En ce qui concerne les
horaires, le règlement intérieur du centre Inria Rennes spécifie par exemple que
vous devez être présente 7~heures par jour de la semaine, en arrivant au plus
tard à 10h et en partant au plus tôt à 16h, avec une pause méridienne d'au moins
45 minutes. En tant que stagiaire, vous ne pouvez pas être au laboratoire le
week-end, ni avant 7h et après 19h en semaine (\textit{i.e.}, quand l'accueil
Inria est fermé).

%\paragraph{Premier stage.}
Votre premier stage peut être déroutant: vous n'avez pas encore l'habitude de travailler en autonomie pendant
plusieurs mois sur le même projet. Le laboratoire va se vider au début de l'été, au départ des juilletistes.
Il reste indispensable de maintenir un rythme de travail raisonnable, même dans la torpeur alanguissant de
l'été. Ne vous y méprenez pas: l'ambiance est décontractée dans les labos, mais celles qui réussissent
travaillent parfois beaucoup. Il n'est cependant pas nécessaire de souffrir pour réussir. L'important est
d'aimer ce qu'on fait.

\vspace{-.5\baselineskip}
\paragraph{Découvrir la recherche.} Une chercheuse passe son temps à réfléchir à
un problème sans même savoir s'il existe une solution. À discuter d'idées plus
ou moins réalistes. À travailler dur pour confirmer des intuitions \textit{parfois}
fondées. À faire fausse piste et se reprendre. À ne pas savoir pourquoi ça
coince, puis à tout recommencer quand elle a enfin compris, quand ça peut
s'écrire proprement.

\vspace{-.5\baselineskip}
\paragraph{Éthique.} On peut se tromper, faire fausse route, mais il ne faut
jamais tricher. Voler des idées, maquiller des résultats ou mal citer ses
sources sont des péchés mortels en recherche. Tricher, c'est reconnaître qu'on
va devoir changer de métier. Ceux pris à tricher n'ont pas droit à une
seconde chance.

\vspace{-.5\baselineskip}
\paragraph{Interagir avec votre équipe.} N'oubliez pas que l'objectif principal
du stage est de découvrir le quotidien de la recherche par la pratique. Vous n'y
parviendrez pas en restant enfermée dans votre bureau du matin au soir. Les
pauses café et les repas sont de bonnes occasions de discuter avec vos collègues
et comprendre leur quotidien, leurs motivations, leurs objectifs.
%
En revanche, limitez autant que possible les pauses avec vos collègues de l'ENS
pendant les heures de travail. Vous pouvez vous retrouver le soir, bien sûr,
mais vous devriez profiter de votre stage pour découvrir le fonctionnement d'une
équipe de recherche.

\vspace{-.5\baselineskip}
\paragraph{Interagir avec votre chercheuse.} Il convient de suivre les conseils
de votre chercheuse. Elle vous aidera à sélectionner et lire la biblio, puis à
mener votre travail de recherche à proprement parler, avant de rédiger le
rapport. Elle ne fera pas votre travail à votre place, mais elle peut répondre à
vos questions. L'impression que vous laissez est très importante, car il est
probable que vous demandiez à votre chercheuse de vous aider à trouver un autre stage
l'an prochain, ou que vous retrouviez ses collègues dans un jury de
recrutement un jour\ldots

\vspace{-.7\baselineskip}
\paragraph{Réunions de travail.} Il est absolument fondamental d'avoir des réunions régulières avec votre
chercheuse. Des réunions quotidiennes de 5 minutes au café ou des discussions ponctuelles par chat ne sont pas
suffisantes. Il faut avoir au moins une heure par semaine (voire deux fois par semaine). Si votre encadrante
ne vous propose pas ces rendez-vous, demandez-lui. Charge à vous de rendre ces réunions intéressantes car
productives. Arrivez avec des choses à dire.

Vous pourrez par exemple commencer par raconter ce que vous avez lu, puis ce que vous avez fait, ce qui marche
et ce qui coince. Ensuite, vous poserez des questions pour débloquer vos problèmes et discutez des directions
futures.  Le luxe est d'envoyer un compte-rendu de la réunion après coup, pour vous assurer que vous êtes
sur la même longueur d'onde que votre encadrante. Les résumés sont fondamentaux pour aider votre encadrante, qui est très multi-tâches.

\vspace{-.7\baselineskip}
\paragraph{Faire un bon stage.}
Emparez vous de votre stage sans attendre que ça tombe tout cuit. Faites ce qui
est convenu entre les réunions, prouvez votre autonomie et votre maturité. Votre
encadrante ne devrait pas avoir besoin de répéter un conseil. Explicitez vos
difficultés et sachez trouver l'aide là où elle se trouve.

\vspace{-.7\baselineskip}
\paragraph{Méthodes infaillibles pour un stage bien raté.} Bloquer sur un
problème pendant trois jours sans en parler; faire trop peu de biblio avant de
se lancer; réinventer la roue de quelque chose qui existe; travailler seule dans
son coin.

\vspace{-.7\baselineskip}
\paragraph{Cahier de laboratoire.}
Outil indispensable pour éviter le pire mal des chercheurs: le trou de mémoire.
Chacune doit trouver sa méthode pour que consigner les informations ne soit pas
trop chronophage tout en assurant que les notes soient exploitables. Incluez les
faits importants (lignes de commandes, URL, notes de réunion ou de lecture
bibliographique). Résumez votre journée avant de rentrer chez vous (choses
faites, points bloquants et problèmes rencontrés, choses à faire ensuite).
Réorganisez le vendredi vos notes de la semaine.

Une bonne méthodologie de travail vous rendra plus efficace. Votre encadrante
n'aura probablement pas le temps de lire votre journal, mais il peut vous faire
gagner énormément de temps. Internet regorge de conseils méthodologiques plus ou
moins pertinents pour les chercheuses en informatique. Des mots-clés à
explorer: \href{https://orgmode.org/}{org-mode},
\href{https://en.wikipedia.org/wiki/Getting_Things_Done}{GTD},
\href{https://www.orgroam.com/}{Zettelkasten \extlink},
\href{https://notes.andymatuschak.org}{Evergreen notes \extlink}. Voir aussi:
\url{http://people.irisa.fr/Martin.Quinson/Research/Students/Methodo/}


\chapter{Après le stage}
\vspace{-2.6\baselineskip}
\section{Le rapport}

\vspace{-.6\baselineskip}
\paragraph{Contenu.}
Comme tous les écrits scientifiques, votre rapport doit répondre à plusieurs
objectifs, en fonction des lecteurs. Il doit permettre à n'importe quelle élève de
L3 de comprendre les objectifs de votre travail, votre méthodologie générale et
vos résultats. Il donnera le cadre général à la stagiaire suivante sur le même
sujet, sans la dispenser de se plonger dans votre code et/ou vos preuves pour
pouvoir étendre votre travail. Il doit également permettre aux collègues de
votre encadrante de comprendre ce que vous avez ajouté à l'état de l'art. Il
sera utilisé par les évaluateurs pour vérifier que vous avez fait un stage
convenable.

\vspace{-.6\baselineskip}
\paragraph{Comment l'écrire.} Rédiger votre rapport à la fin du stage est la
pire stratégie. Commencer à rédiger dès le début dans votre journal aide à mieux
formuler les problèmes, rester concentrée sur l'objectif, et faire plus de
choses plus utiles à l'avancée du stage. Cf.
\href{https://www.microsoft.com/en-us/research/people/simonpj/}{\it Research
  Skills, par Simon Peyton Jones}.

\vspace{-.6\baselineskip}
\paragraph{Conseils.} Tâchez de suivre la forme des articles que vous avez lu.
Vos échecs et votre cheminement importent moins que le résultat obtenu. Le code
et les preuves doivent être en annexe. Faites relire votre rapport par votre
encadrante, de gré ou de force. Il doit donc être prêt à lire bien avant la fin
du stage.

\vspace{-.5\baselineskip}
\paragraph{Format.}
Votre rapport ne peut pas dépasser 6 pages (hors annexes) au
\href{https://www.ieee.org/content/dam/ieee-org/ieee/web/org/conferences/Conference-LaTeX-template_7-9-18.zip}{format
  IEEEtran\extlink} en double colonne, 11pt. Il doit être compréhensible sans lecture des annexes. Il peut
être en français ou en anglais. Choisir l'anglais ne réduit pas les attentes en matière de présentation,
orthographe et grammaire. Si vous n'avez pas la place de tout dire, c'est bon signe. Il sera plus intéressant
à lire.

\vspace{-.6\baselineskip}
\paragraph{Check-list du rapport} pour éviter les problèmes classiques:

\begin{itemize}
\item[$\Box$] Utilisez \textit{ispell} et \textit{LanguageTool}, et relisez votre prose. 
\item[$\Box$] L'introduction précise le domaine, le contexte du travail réalisé et l'objectif.
\item[$\Box$] La démarche et l'incrément par rapport à l'état de l'art sont explicitées.
\item[$\Box$] Les notations sont introduites avant usage, et elles sont cohérentes.
\item[$\Box$] Les figures sont utiles et lisibles, même imprimées en noir et blanc.
\item[$\Box$] La bibliographie est utile, complète, actuelle et de forme cohérente.
\end{itemize}



% Nous aurons notre réunion de "PC Meeting" au début de la semaine prochaine,
% probablement le lundi 4 septembre, à confirmer avec votre emploi du temps de M1.
% Nous discuterons ensemble de la qualité des rapports et des soutenances et nous
% ferons un bilan de cette première expérience de recherche. Je vous demanderai
% aussi de remplir un formulaire d'évaluation spécifique à propos de l'ensemble de
% l'organisation des stages, depuis la choix du sujet jusqu'à la soutenance.

%\url{http://people.irisa.fr/Luc.Bouge/Dokuwiki/doku.php?id=dit:cours:xtra:redaction}

\section{La soutenance}

Les soutenances de stage L3 sont traditionnellement organisées la dernière semaine du mois d'août, avant la
rentrée de M1. Le planning exact vous sera communiqué plus tard.

\vspace{-.5\baselineskip}
\paragraph{Conseils.}
Après un dur labeur, une présentation scientifique est l'occasion de démontrer
vos capacités de chercheuse en partageant vos trouvailles avec d'éminentes
collègues en conférence. L'objectif principal est d'aiguiser la curiosité de
l'auditoire pour gagner des questions intéressantes, voire des collaborations
futures (le graal). C'est un moment crucial pour votre travail et votre
carrière. Vous pouvez laisser l'image d'une oratrice enthousiaste, pédagogue,
apte à transmettre des concepts fondamentaux devant un large public. Si votre
présentation est ratée, que tout le monde s'ennuie, que personne ne comprend ni
l'intérêt du problème résolu, ni l'intuition de votre approche, ni la nature de
votre contribution exacte, et bien~\ldots~ce sera l'effet inverse.

À la rentrée, votre public ne sera pas expert de votre domaine. Mais c'est
toujours ainsi: presque personne ne maîtrise les bases de tous les travaux
présentés dans une conférence donnée. Vous voulez impressionner par vos
capacités de chercheuse ? Faites preuve de pédagogie et rendez compréhensibles
les notions techniques nécessaires à l'appréhension de l'essence de la
contribution.

Nous voulons comprendre le problème de base qui vous a été posé, ainsi que sa
motivation scientifique. Les amis des automates s'attacheront à nous faire
comprendre les applications potentielles de ce problème, tandis que les
développeurs de module noyau insisteront sur les concepts intemporels en jeu. Au
besoin, demandez de l'aide à votre encadrante pour cette partie fondamentale.

Nous voulons comprendre l'approche, la méthodologie que vous avez suivies parmi
les différentes approches possibles. Expliquez-nous comment votre approche est
influencée par le domaine d'expertise de votre équipe d'accueil. C'est peut-être
le moment de citer quelques travaux fondateurs dans ce domaine.

Nous voulons comprendre votre contribution, comment vous avez (partiellement)
répondu au problème posé. Faites une distinction nette entre ce qui était déjà
là avant le début de votre stage et ce qui constitue votre contribution propre:
le fruit de votre collaboration avec votre encadrante pendant le stage.

Enfin, la recherche s'attaque aux problèmes difficiles: faites-nous
\textit{comprendre} la difficulté du vôtre. Mais si vous voulez vraiment nous
impressionner, il ne suffit pas de dire que c'est dur. Il faut nous l'expliquer
de façon compréhensible. Nous bombarder d'équations ou diagrammes
incompréhensibles est contre-productif. Ne montrez pas plus d'une règle de
réécriture/règle de sémantique/en-tête de procédure/etc, et expliquez-la bien.
Pour les autres, dites "c'est pareil".


\newpage\paragraph{Format.} Chaque soutenance dure 15 mn: 10 mn d'exposé, 4 mn de
questions, 1 minute de battement. Il serait déraisonnable de préparer plus d'une
diapo utile par minute, soit 10 diapos dans ce cas. Quelques indications
classiques:
\begin{itemize}
\item Votre présentation doit être soignée : vérifiez l'orthographe, ajoutez des
  illustrations (en évitant les captures d'écran pixelisée, les schémas vides et
  les équations trop touffues pour être détaillées). Utilisez de la couleur.
\item C'est un support de votre discours: vous direz plus de choses que ce qui
  est écrit, mais chaque planche doit être relativement autosuffisante pour
  qu'on puisse comprendre l'objectif de chacune d'elle sans le son. Les détails
  sont dans le rapport, pas besoin de tout mettre dans les diapos.
\item Mieux vaut en dire moins en parlant à vitesse normale, plutôt manger ses
  mots, stresser et bafouiller. Vous devez être compréhensible.
\item Regardez le jury, ou au pire l'écran de l'ordinateur face au public, mais
  ne tournez pas le dos. C'est plus agréable pour l'auditoire et cela vous
  aidera à savoir si une explication mérite d'être reformulée.
\item Répétez votre présentation devant votre équipe de recherche si possible.
\item Respectez scrupuleusement le temps: c'est un critère d'évaluation facile.
\item Le video-projecteur n'est \textbf{pas} votre ami : faites un essai préalable.
\item Répétez votre présentation (une 2e fois ça ne vous fera pas de mal).
\end{itemize}

\paragraph{Quelques liens supplémentaires.}
\begin{itemize}
\item[$\bullet$] La page de méthodologie de recherche par Simon Peyton Jones,
  citée ci-dessus, comporte une section sur les présentations scientifiques.
\item[$\bullet$] \textit{Scientific Writing for Computer Science Students},
  Wilhelmiina Hämäläinen. 130 pages de conseils, avec également des éléments de
  grammaire anglaise pour non-anglophone et d'autres sur le bon usage de \LaTeX.
\item[$\bullet$] \href{https://www.ohsu.edu/sites/default/files/2018-12/Zuckerman-Science-Writing-Checklist.pdf}{\textit{Science Writing Checklist}\extlink}, par D. Zuckerman.
  Check-list fort utile, même si elle s'applique mieux aux sciences naturelles
  qu'à l'informatique (les sections Méthodes, Résultats et Discussion sont
  absentes chez nous).
\item[$\bullet$] \href{http://www.octares.com/14-telechargements-gratuits}{\textit{L'orthographe n'est pas
      soluble dans les études supérieures\extlink} François Daniellou. Aide-mémoire bienveillant à l'usage des
    étudiants,} très complet pour le français, lisible sur téléphone.
\end{itemize}

\medskip

Indiquez moi tout autre lien intéressant à citer ici ou sur la page suivante:\\
\centerline{\url{http://people.irisa.fr/Martin.Quinson/Research/Students/Links}}

\end{document}

%  Local Variables:
%  coding: utf-8
%  End:

 
% LocalWords:  encadrante encadrantes Inria CNRS
