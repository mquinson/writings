\documentclass[openany]{book}

\usepackage{color}
\usepackage{polyemptty}

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[french]{babel}

% \usepackage{ifthen,amsmath,amstext,amssymb}
% \usepackage{textcomp} % so that the euro symbol works
% \usepackage{longtable,float,wrapfig,subfigure}
\usepackage{url} \urlstyle{sf}
\usepackage{graphics}
\usepackage{amsfonts}
\usepackage[hidelinks]{hyperref}
\usepackage{enumitem}
\setitemize{leftmargin=1.4em,noitemsep,topsep=0pt,parsep=2pt,partopsep=0pt}

\titleformat{\chapter}[display]
  {\color{violet}\startcontents\normalfont\huge\bfseries}
  {\vspace{-5ex} Partie \Alph{chapter}}
  {0pt}
  {\Huge}
\renewcommand{\thechapter}{\Alph{chapter}}  

   \begin{document}
\noindent
\begin{minipage}{.95\linewidth}
\begin{center}

  {\huge\color{violet}\bf Étude bibliographique efficace}\smallskip

  {\footnotesize Lire en ligne: \url{https://framagit.org/mquinson/writings/blob/master/stage-L3/biblio.pdf}}

  Martin Quinson
\end{center}
\end{minipage}~\begin{minipage}{.05\linewidth}
  \rotatebox{90}{\footnotesize v220911}
\end{minipage}

\bigskip%\bigskip

Ce document présente une méthodologie\footnote{Ce document vient en annexe du
  \textbf{\color{violet}\href{https://framagit.org/mquinson/writings/-/blob/master/stage-L3/stagiaire.pdf}{guide
      de l'ex-future stagiaire}},
  \href{https://framagit.org/mquinson/writings/-/blob/master/stage-L3/stagiaire.pdf}{à
    lire en ligne\includegraphics[height=.8\baselineskip]{img/extlink.pdf}}.} à
appliquer quand on lit un article pour l'étude bibliographique d'un travail de
recherche. C'est adapté au travail d'un stage ou d'une thèse, mais ce n'est pas
adapté au reviewing où l'on cherche à juger si l'article étudié mérite
\textit{dans l'absolu} d'être publié.

%\medskip

L'important lors de l'étude bibliographique est de {\color{violet}faire avancer
  ses recherches}. Il faut choisir quoi lire, savoir comment le lire et comment
assimiler la substance de la bibliographie étudiée. On peut en profiter pour
{\color{violet}observer des articles bien écrits}, et apprendre à connaître sa
communauté scientifique. Pas simple au début, mais ces questions vous guideront.
\href{https://framagit.org/mquinson/writings/-/raw/master/stage-L3/biblio.txt?inline=false}{Complétez
  le template \includegraphics[height=.8\baselineskip]{img/extlink.pdf}}.


\vspace{-3.8\baselineskip}
% Table des matières générales, et restaure le style de chapitre ensuite
\begingroup
\let\cleardoublepage\relax
\let\clearpage\relax
\renewcommand\contentsname{\vspace{-15mm}}
\vspace{-3\baselineskip}
\tableofcontents
\endgroup

\chapter{Lire des articles en pratique}
\vspace{-3.5\baselineskip}

Voici quelques pistes qui peuvent guider la lecture d'articles scientifiques.
L'objectif n'est pas de suivre cette méthodologie à la lettre et à toutes les
questions pour chaque article, mais plutôt de picorer ce qui est pertinent pour vous.

Il faut cibler ses efforts pour être efficace: impossible d'étudier tous les
articles intéressants. Pour \textbf{\color{violet}éviter la noyade
  bibliographique}, il faut constamment se demander si cette lecture est
vraiment pertinente pour mes recherches, et passer à la suite si non. Ne
répondre qu'à certaines questions, ou n'étudier qu'une partie d'un article donné
est une bonne idée quand on peut l'argumenter.

\vspace{-\baselineskip}
\section{Lire un article en trois passes}
\vspace{-2.1\baselineskip}
\hfill [Keshav]\hspace{2cm}

\vspace{-\baselineskip}
\paragraph{Première passe.}
Scan en 5 à 10 minutes pour se faire une première idée de l'article et son
contexte. On ne lit que le titre, le résumé, l'intro, les titres de sections et
la conclusion en ignorant le contenu des autres sections. On parcourt les
références pour tenter de situer le travail dans ce qu'on a déjà lu. Après cette
passe, on doit pouvoir répondre aux questions de la section \ref{sec:promesses}.

\vspace{-.9\baselineskip}
\paragraph{Seconde passe.} Analyse en une heure pour être capable de résumer
l'article et son argumentaire à un collègue. Lecture complète (sauf les
preuves), en annotant dans la marge. Les figures et diagrammes méritent une
attention particulière, et il faut noter les références bibliographiques à
explorer plus tard.

\vspace{-.9\baselineskip}
\paragraph{Troisième passe.} Analyse en une demi-journée pour une compréhension
en profondeur. Reconstruisez l'article mentalement, en faisant le même
cheminement mental que les auteurs: mêmes suppositions (à identifier), mêmes
arguments (à challenger). Réfléchissez à comment vous auriez réalisé chaque
point, à votre façon. Après cette passe, on peut reconstruire l'article de
mémoire, discuter ses points forts et ses faiblesses (suppositions implicites,
citations manquantes à des travaux comparables, problèmes expérimentaux ou
analytiques).

\vspace{-.7\baselineskip}
\section{Découvrir un domaine \textit{(lit. review)}}
\vspace{-2.1\baselineskip}
\hfill [Keshav]
\smallskip

Pour vous construire une compréhension globale d'un nouveau domaine, cherchez 3
à 5 articles \textit{récents} du domaine avec CiteSeer ou Google Scholar.
Parcourez chaque article rapidement (première passe), puis croisez les sections
"Related work" de tous les articles pour comprendre l'état actuel du domaine.

Les bons \textit{survey} sont rares, mais très utiles. Cherchez les références
communes aux articles que vous avez sélectionné, identifiez les auteurs actifs
sur ce thème et les endroits où ils publient.
\chapter{Analyser un article}
\vspace{-3\baselineskip}

\section{L'article et son contexte}\label{sec:contexte_article}

\vspace{-.5\baselineskip}
\paragraph{Quoi lire?}
C'est la question fondamentale d'une étude bibliographique. Au début du stage,
on lira les articles du sujet de stage, mais il faut ensuite savoir choisir quoi
lire, et surtout quoi NE PAS lire (c'est la
question~\ref{sec:quoi_lire_ensuite}). En régime permanent, j'apprécie que mes
étudiants envisagent plusieurs articles chaque semaine, pour en choisir un à me
présenter en profondeur. Cela évite que la TOREAD ne se remplisse trop, tout en
avançant l'étude bibliographique.

\vspace{-\baselineskip}
\paragraph{Pourquoi le lire?}
Si on ne sait pas ce qu'on veut retirer d'un article, la lecture sera
inefficace. Est-ce pour mieux comprendre mon sujet? Pour renforcer ma
connaissance d'un domaine de recherche connexe au mien? Pour mieux cerner les
travaux existants et trouver comment améliorer l'état de l'art?

\vspace{-\baselineskip}
\paragraph{Prendre des notes.} Votre journal doit vous permettre de réutiliser
vos lectures plusieurs mois après, sans devoir tout relire. Conservez également
le pdf pour référence. Utilisez (et ajustez!) vos notes lors de la réunion
hebdomadaire où vous alignez votre compréhension de l'article étudié avec celle
de votre encadrant.


\vspace{-.5\baselineskip}
\subsection{Promesse de l'article}\label{sec:promesses}
\vspace{-.2\baselineskip}

\begin{itemize}
\item[$\bullet$] Quel est l'objectif général de ce travail? 
\item[$\bullet$] De quoi se vantent les auteurs dans le titre, le résumé,
  l'intro et la conclusion ?
\item[$\bullet$] En quoi est-ce lié à mon travail? Pour quoi devrais-je le lire?
\item[$\bullet$] L'article a-t-il l'air bien écrit et clair? Le travail semble-t-il convainquant?
\end{itemize}

\vspace{-.5\baselineskip}
\paragraph{GO/NOGO:} Faut-il que je lise cet article en profondeur, ou ai-je un
autre article plus intéressant à lire pour mon travail ?

\vspace{-.5\baselineskip}
\subsection{D'où parlent les auteurs ? De quel point de vue ?}
\vspace{-.2\baselineskip}
\begin{itemize}
\item[$\bullet$] \textbf{\color{violet}Qui} sont chacun des auteurs (doctorant,
  permanent)?\\ Quels sont les travaux antérieurs de chacun?\\ Quelle est la
  contribution probable de chacun au travail présenté?
\item[$\bullet$] \textbf{\color{violet}Où} est-ce publié (conférence/journal) ?\\
  Qu'est ce qu'on trouve habituellement là-bas?
\item[$\bullet$] \textbf{\color{violet}Contexte}: Quels articles de la
  bibliographie ai-je déjà lu?
\end{itemize}

\section{Lire pour construire ma contribution}

\vspace{-.5\baselineskip}
\subsection{Nature de la contribution}

\begin{itemize}
\item[$\bullet$] Comment les auteurs font-ils pour atteindre l'objectif affiché?
  \begin{itemize}
  \item[$\circ$] Est-ce une preuve, un algo, une simulation, un outil?
  \end{itemize}
\item[$\bullet$] Quelles sont les bonnes idées?
  \begin{itemize}
  \item[$\circ$] Qu'est ce qui a dû leur prendre du temps à faire?
  \item[$\circ$] Qu'aurait-on pu faire autrement en mieux?
  \end{itemize}
\end{itemize}

%\vspace{-.5\baselineskip}
\subsection{Pourquoi ça marche?}
%\vspace{-.7\baselineskip}
\begin{itemize}
\item[$\bullet$] Quel est le modèle sous-jacent? Quelles sont les hypothèses
  de base?
  \begin{itemize}
  \item[$\circ$] Est-ce crédible / réaliste / classique?
  \item[$\circ$] Impact sur la contribution si on changeait un peu ces hypothèses?
  \end{itemize}
\item[$\bullet$] Quelles sont les conditions d'évaluation (cas d'usage,
  workload, etc)?
  \begin{itemize}
  \item[$\circ$] Est-ce crédible / réaliste / classique?
  \item[$\circ$] Impact probable sur les résultats si on changeait ces conditions?
  \end{itemize}
\end{itemize}

%\vspace{-.5\baselineskip}
\subsection{Résultats effectifs}
%\vspace{-.7\baselineskip}
\begin{itemize}
\item[$\bullet$] Qu'ont ils fait au fond?
  \begin{itemize}
  \item[$\circ$] Comment cela se compare à ce qu'ils promettaient au début?
  \item[$\circ$] Est-ce impressionnant par rapport aux travaux comparables (dont
    les miens)?
  \end{itemize}
\item[$\bullet$] Où sont les limites de l'approche ?
\end{itemize}

\vspace{-.5\baselineskip}
\subsection{Partie évaluation}
% \vspace{-.7\baselineskip}
\begin{itemize}
\item[$\bullet$] Est-ce que le descriptif est assez clair pour refaire les
  expériences?
  \begin{itemize}
  \item[$\circ$] Les artéfacts sont-ils assez documentés pour être
    compréhensibles?
  \end{itemize}
\item[$\bullet$] Les promesses de l'article sont-elles démontrées par l'évaluation?
  \begin{itemize}
  \item[$\circ$] Quelle est le rôle de chaque expérience dans l'argumentaire?
  \item[$\circ$] Quelle expérience manque pour que le papier soit convainquant?
  \end{itemize}
\end{itemize}

% \vspace{-.5\baselineskip}
\subsection{Ce que ça m'apprend, ce que je garde}
%\vspace{-.7\baselineskip}
Idée ou nouvelle piste de recherche ou idée, motivation en intro de mes
articles, champ d'application de mes travaux précédents, curiosité doute ou
supposition quelconque, Algo auquel me comparer, belle expérience pour valider
ma propre contribution, concurrent à citer dans le SotA, ...

\newpage
\section{Forme de mes futurs articles}

%\vspace{-2.8\baselineskip}
\paragraph{Motivation.} La forme d'un article scientifique est très codifiée,
même si chaque communauté a ses propres règles implicites. Il faut profiter de
ses lectures pour décoder ces règles, et étudier comment présenter efficacement
une contribution.

\vspace{-\baselineskip}
\paragraph{En pratique.} Lorsque vous trouvez un article particulièrement clair,
observez les techniques et habitudes des rédacteurs de votre communauté,
discutez-les avec votre encadrant, mais sans perdre trop de temps à les
commenter par écrit.


\vspace{-.5\baselineskip}
\subsection{Plan général de l'article}
%\vspace{-.7\baselineskip}
Identifiez les sections contribution, évaluation, état de l'art (SotA en
anglais) et observez leur ordre. Si ce plan diverge de ce qu'on trouve ailleurs,
pourquoi ?

\vspace{-.5\baselineskip}
\subsection{Structure de chaque partie}
%\vspace{-.2\baselineskip}
\begin{itemize}
\item[$\bullet$] Quelle est la structure de l'introduction ?
  \begin{itemize}
  \item[$\circ$] Quelle est la fonction dans l'argumentaire de chaque paragraphe ?
  \item[$\circ$] Est-ce que l'introduction va bien du générique au spécifique?
  \item[$\circ$] Est-ce que toutes les notions sont introduites avant usage ?
  \end{itemize}
\item[$\bullet$] Comparez la structure de l'introduction à celle de la conclusion.
\item[$\bullet$] Dans la partie évaluation, identifiez les parties matériel,
  méthode, résultats et discussion.
\item[$\bullet$] Quelle est la fonction dans l'argumentaire de chaque phrase du
  résumé?\\ Que pouvait-on enlever de ce résumé ?
\item[$\bullet$] Pouvez-vous trouver un meilleur titre pour cet article ?
\end{itemize}

\vspace{-.5\baselineskip}
\subsection{Communauté scientifique ciblée}
%\vspace{-.7\baselineskip}
\begin{itemize}
\item[$\bullet$] À quelle communauté scientifique est destiné cet article?
  \begin{itemize}
  \item[$\circ$] Notions explicitées dans le contexte et celles laissées
    implicites.
  \item[$\circ$] Où est-ce publié ?
  \end{itemize}
\item[$\bullet$] Que faudrait-il modifier pour une présentation dans d'autres
  communautés proches que vous connaissez ?
\end{itemize}

\newpage
\section{Que faire après cette lecture?}

\vspace{-.5\baselineskip}
\subsection{Qu'est ce que j'en fais?}

Attention, cette question peut prendre plusieurs mois à temps plein, voire des
années. On ne peut donc travailler en profondeur qu'un nombre très limité
d'articles. Mais quand on choisit bien, c'est extrêmement productif.

\medskip
\begin{itemize}
\item[$\bullet$] Quelle question aurais-je envie de poser aux auteurs pour aller
  plus loin ? Ils y ont passé du temps, et adoreraient probablement voir des
  gens s'y intéresser. Le tout est d'éviter l'ego trip "vous devriez citer mon
  travail" :)
\item[$\bullet$] Est-ce que leur code est téléchargeable? Si non, est-ce que je
  peux leur demander ? À quoi il ressemble? Est-ce que j'arrive à le relancer ?
\item[$\bullet$] Puis-je appliquer l'approche dans un autre contexte, avec d'autres hypothèses ?
\item[$\bullet$] Est-ce que je peux intégrer le même genre d'idées dans ce que je fais ?
\end{itemize}


\vspace{-.5\baselineskip}
\subsection{Que lire ensuite?}\label{sec:quoi_lire_ensuite}

Une lecture intéressante me permet d'ajouter de nouvelles choses à ma liste de
choses à lire, à évaluer pour la prochaine étape de l'étude bibliographique.
% 
Plusieurs sources à considérer:

\begin{itemize}
\item[$\bullet$] {\color{violet}Bibliographie ascendante:} articles plus anciens cités par cet article.
\item[$\bullet$] {\color{violet}Bibliographie descendante}
  \href{https://scholar.google.com/}{(avec Google Scholar)~:} les articles plus récents citant cet article
  sont l'activité actuelle de ceux qui s'intéressent à ce sujet.
\item[$\bullet$] {\color{violet}Bibliographie du domaine} \href{https://dblp.uni-trier.de/}{(avec DBLP)~:}
  articles des mêmes auteurs et de leurs co-auteurs habituels, ou de cette
  conférence les autres années.
\end{itemize}

\paragraph{Liens supplémentaires.} Partagez-moi vos liens intéressants à citer ici, svp.

\smallskip
\begin{itemize}
\item[$\bullet$] Efficient Reading of Papers in Science and Technology, Michael J. Hanson, Dylan J. McNamee
  (\href{http://people.irisa.fr/Martin.Quinson/Research/Students/efficientReading.pdf}{accéder au pdf}).
\item[$\bullet$] How to Read a Paper, S. Keshav
  (\href{http://ccr.sigcomm.org/online/files/p83-keshavA.pdf}{accéder au pdf}).
\end{itemize}

\vspace{-.5\baselineskip}
\paragraph{À propos de ce document.} 
La mise en page en A5 permet d'imprimer en deux pages par page sans s'abîmer les
yeux. Toute idée d'amélioration est bienvenue. Ce document est distribué sous
licence CC-BY-SA. Vous pouvez le diffuser et l'améliorer, mais sans restreindre
les droits de vos lecteurs.\\ Merci aux collègues (Anne-Cécile Orgerie,
Emmanuelle Saillard) et les étudiants qui m'ont aidé à affiner cette
méthodologie.


\end{document}

%  Local Variables:
%  coding: utf-8
%  End:

 
% LocalWords:  encadrante encadrantes Inria CNRS
